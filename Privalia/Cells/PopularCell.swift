//
//  File.swift
//  Privalia
//
//  Created by Nil Puig on 27/03/2018.
//  Copyright © 2018 Nil Puig. All rights reserved.
//

import UIKit

public final class PopularCell: UICollectionViewCell {
    
    @IBOutlet weak var movieImageView: UIImageView!
    
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        movieImageView.image = nil
    }
    
    override public func prepareForReuse() {
        super.prepareForReuse()
        movieImageView.image = nil
    }
}
