//
//  File.swift
//  Privalia
//
//  Created by Nil Puig on 27/03/2018.
//  Copyright © 2018 Nil Puig. All rights reserved.
//

import UIKit

public final class BrowseCell: UITableViewCell {
    
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        movieImageView.image = nil
    }
    
    override public func prepareForReuse() {
        super.prepareForReuse()
        movieImageView.image = nil
    }
}
