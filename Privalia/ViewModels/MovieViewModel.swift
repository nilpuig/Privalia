//
//  MovieViewModel
//  Privalia
//
//  Created by Nil Puig on 28/03/2018.
//  Copyright © 2018 Nil Puig. All rights reserved.
//


import Foundation

struct MovieViewModel {
    let poster_path: String // Movie poster url
    let title: String
    let overview: String
    let release_date: String
    
    
    init(movie: Movie) {
        poster_path = movie.poster_path
        title = movie.title
        overview = movie.overview
        release_date = movie.release_date
    }
}
