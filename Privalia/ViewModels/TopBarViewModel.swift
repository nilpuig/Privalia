//
//  TopBar.swift
//  Privalia
//
//  Created by Nil Puig on 27/03/2018.
//  Copyright © 2018 Nil Puig. All rights reserved.
//

import UIKit

class TopBar: UIView {
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.shadowRadius = 8
        layer.shadowOffset = CGSize(width: 0, height: 3)
        layer.shadowColor = Color.shadow.value.cgColor
        layer.shadowOpacity = 0.37
    }
}
