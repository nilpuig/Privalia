//
//  ViewController.swift
//  Privalia
//
//  Created by Nil Puig on 25/03/2018.
//  Copyright © 2018 Nil Puig. All rights reserved.
//

import UIKit
import SwiftyJSON
import PINRemoteImage

class Popular: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    let cellIdentifier = "PopularCell"
    var cellSize = CGSize()
    private var isReloading = false
    private let isTesting = false
    private let RELOAD_TIME_MEMORY_WARNING = 4.0 // Waiting time to reload collection view when there's a memory warning
    private var canReload = true  // Current waiting time to reload collection view
    private let movieViewModelController = MovieViewModelController()
    
    let maxDownloads = 20 // max number of downloads per second
    var downloads = 0 // number of downloads
    var initialTime = 0.0 // Time when first download started
    var currentTime = 0.0
    var reloadTime = 0.0 // Check if collection data reload can take place

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialTime = Double(Calendar.current.component(.second, from: Date()))
        
        // Register cell for MoviesCollectionView
        collectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        
        movieViewModelController.apiRoute = API.ROUTE_POPULAR
        movieViewModelController.retrieveMovies { [weak self] (success, error) in
            guard let strongSelf = self else { return } // strongify optional self
            if !success {
                DispatchQueue.main.async {
                    let title = "Error"
                    if let error = error {
                        strongSelf.showError(title, message: error.localizedDescription)
                    } else {
                        strongSelf.showError(title, message: NSLocalizedString("Can't retrieve data", comment: "Can't retrieve data"))
                    }
                }
            } else {
                if (strongSelf.isTesting) {
                    print("Success | retrieveMovies")
                }
                
                DispatchQueue.main.async {
                    // Ensure we don't download too many images at a time
                    var waitingTime = 0.0
                    let timeNow = Double(Calendar.current.component(.second, from: Date()))
                    if(timeNow - strongSelf.reloadTime < 6) {
                        waitingTime = strongSelf.RELOAD_TIME_MEMORY_WARNING
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + waitingTime, execute: {
                        strongSelf.canReload = true
                        strongSelf.isReloading = true
                        strongSelf.collectionView?.reloadData()
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                            strongSelf.isReloading = false
                        })
                    })
                }
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieViewModelController.viewModelsCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! PopularCell
        
        // If collection view is reloading, don't change alpha of images to ensure good user experience
        if (!isReloading) {
            cell.movieImageView.alpha = 0.0
        }

        if let viewModel = movieViewModelController.viewModel(at: indexPath.row) {

            currentTime = Double(Calendar.current.component(.second, from: Date()))
            if (downloads >= maxDownloads) {
                if (currentTime - initialTime < 2) {
                    print("Delay collection reloadData")
                    reloadTime = currentTime + 5
                }
                initialTime = currentTime
                downloads = 0
            }
            
            cell.movieImageView.pin_updateWithProgress = true
            cell.movieImageView.pin_setImage(from: URL(string: viewModel.poster_path)!)
            
            downloads += 1
        }
        
        if (!isReloading) {
            UIView.animate(withDuration: 1.1, animations: {
                cell.movieImageView.alpha = 1.0
            })
        }
        
        if (isTesting) {
            print("cellForRowAt: \(indexPath.row)")
        }
        
        return cell
    }

    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if (isTesting) {
            print("willDisplay: \(indexPath.row)")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let columns: Int = {
            var count = 2
            if traitCollection.horizontalSizeClass == .regular {
                count = count + 1
            }
            if collectionView.bounds.width > collectionView.bounds.height {
                count = count + 1
            }
            return count
        }()
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(columns - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(columns))
        return CGSize(width: size, height: 250)
    }
    
    var isAlertDisplayed = false
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.

        print("didReceiveMemoryWarning")

        canReload = false
        if (!isAlertDisplayed) {
            isAlertDisplayed = true
            self.presentAlert(title:"Loading")
            
            DispatchQueue.main.asyncAfter(deadline: .now() + self.RELOAD_TIME_MEMORY_WARNING, execute: {
                self.dismissCustomAlert()
                self.isAlertDisplayed = false
            })
        }
    }
}






