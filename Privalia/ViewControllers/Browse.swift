//
//  ViewController.swift
//  Privalia
//
//  Created by Nil Puig on 25/03/2018.
//  Copyright © 2018 Nil Puig. All rights reserved.
//

import UIKit

class Browse: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var loupeView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loupeImageView: UIImageView!
    @IBOutlet weak var loupeLabel: UILabel!
    
    private var movieViewModelController = MovieViewModelController()
    let cellIdentifier = "BrowseCell"
    private var isReloading = false
    private let isTesting = false
    private var canReload = true  // Current waiting time to reload collection view
    private let RELOAD_TIME_MEMORY_WARNING = 4.0 // Waiting time to reload collection view when there's a memory warning
    let maxDownloads = 20 // max number of downloads per second
    var downloads = 0 // number of downloads
    var initialTime = 0.0 // Time when first download started
    var currentTime = 0.0
    var reloadTime = 0.0 // Check if collection data reload can take place
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Register cell for BrowsetableView
        tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieViewModelController.viewModelsCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! BrowseCell
        
        // If collection view is reloading, don't change alpha of images to ensure good user experience
        if (!isReloading) {
            cell.movieImageView.alpha = 0.0
            cell.titleLabel.alpha = 0.0
            cell.yearLabel.alpha = 0.0
            cell.overviewLabel.alpha = 0.0
        }
        
        if let viewModel = movieViewModelController.viewModel(at: indexPath.row) {
            
            currentTime = Double(Calendar.current.component(.second, from: Date()))
            if (downloads >= maxDownloads) {
                if (currentTime - initialTime < 2) {
                    print("Delay")
                    reloadTime = currentTime + 5
                }
                initialTime = currentTime
                downloads = 0
            }
            
            cell.movieImageView.pin_updateWithProgress = true
            cell.movieImageView.pin_setImage(from: URL(string: viewModel.poster_path)!)
            cell.titleLabel.text = viewModel.title
            cell.yearLabel.text = viewModel.release_date
            cell.overviewLabel.text = viewModel.overview
            
            downloads += 1
        }
        
        if (!isReloading) {
            UIView.animate(withDuration: 0.4, animations: {
                cell.movieImageView.alpha = 1.0
                cell.titleLabel.alpha = 1.0
                cell.yearLabel.alpha = 1.0
                cell.overviewLabel.alpha = 1.0
            })
        }
        
        if (isTesting) {
            print("cellForRowAt: \(indexPath.row)")
        }
        
        return cell
    }
    
    let MinHeight: CGFloat = 100.0
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let tableHeight = tableView.bounds.height
        return tableHeight
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (isTesting) {
            print("willDisplay: \(indexPath.row)")
        }
    }
    
    var isAlertDisplayed = false
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        print("didReceiveMemoryWarning")
        
        canReload = false
        if (!isAlertDisplayed) {
            isAlertDisplayed = true
            self.presentAlert(title:"Loading")
            
            DispatchQueue.main.asyncAfter(deadline: .now() + self.RELOAD_TIME_MEMORY_WARNING, execute: {
                self.dismissCustomAlert()
                self.isAlertDisplayed = false
            })
        }
    }
    
    
    /*  Search bar */
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("textDidChange")
        
        var trimmedText = searchText.replacingOccurrences(of: " ", with: "")
        trimmedText = searchText.trimmingCharacters(in: .illegalCharacters)
        
        print(trimmedText)
        
        if (trimmedText == "" ) {
            return
        }
        
        loupeView.isHidden = true
        
        initialTime = Double(Calendar.current.component(.second, from: Date()))
        
        movieViewModelController = MovieViewModelController()
        movieViewModelController.searchTerm = trimmedText
        movieViewModelController.apiRoute = API.ROUTE_SEARCH
        
        movieViewModelController.retrieveMovies { [weak self] (success, error) in
            guard let strongSelf = self else { return } // strongify optional self
            if !success {
                if (strongSelf.isTesting) {
                    print("Error | retrieveMovies")
                }
                return
            } else {
                if (strongSelf.isTesting) {
                    print("Success | retrieveMovies")
                }
                
//                strongSelf.tableView?.reloadData()
                DispatchQueue.main.async {
                    // Ensure we don't download too many images at a time
                    var waitingTime = 0.0
                    let timeNow = Double(Calendar.current.component(.second, from: Date()))
                    if(timeNow - strongSelf.reloadTime < 6) {
                        waitingTime = strongSelf.RELOAD_TIME_MEMORY_WARNING
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + waitingTime, execute: {
                        strongSelf.canReload = true
                        strongSelf.isReloading = true
                        strongSelf.tableView?.reloadData()
                        print("RELOAD")
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                            strongSelf.isReloading = false
                        })
                    })
                }
            }
        }
    }
    
    // Make sure the table view cell is always centered
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        if (isReloading) {
//            return
//        }
//        let tableHeight = tableView.bounds.height
//        let rowPosition = (scrollView.contentOffset.y / tableHeight).rounded()
//
//
////        let indexPath = NSIndexPath(item: rowPosition, section: 0) // Current indexPath that is visible
////        self.tableView.scrollToRow(at: indexPath as IndexPath, at: UITableViewScrollPosition.middle, animated: true)
////        targetContentOffset->y = floorf(targetContentOffset->y / self.rowHeight) * self.rowHeight
//
////        UIView.animate(withDuration: 3.0, animations: {
////            self.tableView.scrollToRow(at: self.currentIndexPath as IndexPath, at: UITableViewScrollPosition.middle, animated: true)
////
////        })
//
//    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
    }

}

