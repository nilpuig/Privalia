//
//  File.swift
//  Privalia
//
//  Created by Nil Puig on 27/03/2018.
//  Copyright © 2018 Nil Puig. All rights reserved.
//


import UIKit

extension UIViewController {
    func showError(_ title: String, message: String) {
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func presentAlert(title: String) {
        let nib = UINib(nibName: "AlertLoadingView", bundle: nil)
        let customAlert = nib.instantiate(withOwner: self, options: nil).first as! AlertLoadingView
        
        customAlert.tag = 200
        customAlert.title.text = title
        customAlert.indicator.startAnimating()
        customAlert.backgroundColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.2)
        
        let screen = UIScreen.main.bounds
        customAlert.center = CGPoint(x: screen.midX, y: screen.midY)
        
        self.view.addSubview(customAlert)
    }
    
    func dismissCustomAlert() {
        if let view = self.view.viewWithTag(200) {
            view.removeFromSuperview()
        }
    }
}
