//
//  File.swift
//  Privalia
//
//  Created by Nil Puig on 27/03/2018.
//  Copyright © 2018 Nil Puig. All rights reserved.
//

import UIKit

extension UITabBar {
//    override open func sizeThatFits(_ size: CGSize) -> CGSize {
//        var sizeThatFits = super.sizeThatFits(size)
//        sizeThatFits.height = 50
//        return sizeThatFits
//    }
//    
    override open func awakeFromNib() {
        super.awakeFromNib()
        layer.shadowOffset = CGSize(width: 0, height: -3)
        layer.shadowRadius = 8
        layer.shadowColor = Color.shadow.value.cgColor
        layer.shadowOpacity = 0.37
    }
}

