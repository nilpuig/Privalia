//
//  File.swift
//  Privalia
//
//  Created by Nil Puig on 27/03/2018.
//  Copyright © 2018 Nil Puig. All rights reserved.
//

import UIKit

extension UITabBarItem {
    override open func awakeFromNib() {
        super.awakeFromNib()
        titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -2)
    }
}

