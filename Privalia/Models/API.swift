//
//  API
//  Privalia
//
//  Created by Nil Puig on 26/03/2018.
//  Copyright © 2018 Nil Puig. All rights reserved.
//

import Foundation
import SwiftyJSON

struct API {
    
    public static let API_KEY = "93aea0c77bc168d8bbce3918cefefa45"
    
    public static let BASE_API_URL = "https://api.themoviedb.org/3"
    
    public static let ROUTE_POPULAR = "/movie/popular"
    
    public static let ROUTE_SEARCH = "/search/movie"
    
    public static let BASE_IMAGE_URL = "https://image.tmdb.org/t/p/w1280"
    
    public static let PARAMETERS = "api_key=93aea0c77bc168d8bbce3918cefefa45&language=en-US"
    
    public static let PARAMETERS_SEARCH = "include_adult=false&query="
    
    public static let TEST_URL_POPULAR = "https://api.themoviedb.org/3/movie/popular?api_key=93aea0c77bc168d8bbce3918cefefa45&language=en-US&page=1"
    
    public static let TEST_URL_SEARCH = "https://api.themoviedb.org/3/search/movie?api_key=93aea0c77bc168d8bbce3918cefefa45&language=en-US&query=hola&page=1&include_adult=false"
    
    enum JSONError: String, Error {
        case NoData = "ERROR: no data"
        case ConversionFailed = "ERROR: conversion from JSON failed"
    }
}

