//
//  Movie
//  Privalia
//
//  Created by Nil Puig on 28/03/2018.
//  Copyright © 2018 Nil Puig. All rights reserved.
//


import Foundation

struct Movie: Codable {
    enum CodingKeys: String, CodingKey {
        case poster_path = "poster_path"
        case title = "title"
        case overview = "overview"
        case release_date = "release_date"
    }

    let poster_path: String // Movie poster url
    let title: String
    let overview: String
    let release_date: String
    
    init(poster_path: String, title: String, release_date: String, overview: String) {
        self.poster_path = poster_path
        self.title = title
        self.overview = overview
        self.release_date = release_date
    }
}
