//
//  MovieViewModelController
//  Privalia
//
//  Created by Nil Puig on 28/03/2018.
//  Copyright © 2018 Nil Puig. All rights reserved.
//

import Foundation
import SwiftyJSON

typealias RetrieveMoviesCompletionBlock = (_ success: Bool, _ error: NSError?) -> Void

class MovieViewModelController {
    private static let pageSize = 20
    private let apiBaseURL = "https://api.themoviedb.org/3"
    private let popularMoviesRoute = "/movie/popular"
    private var viewModels: [MovieViewModel?] = []
    private var currentPage = -1 // Page number of popular movies to be fetched
    private var retrieveMoviesCompletionBlock: RetrieveMoviesCompletionBlock?
    var searchTerm = ""
    var apiRoute = API.ROUTE_POPULAR

    func retrieveMovies(_ completionBlock: @escaping RetrieveMoviesCompletionBlock) {
        retrieveMoviesCompletionBlock = completionBlock
        loadNextPageIfNeeded(for: 0)
    }

    var viewModelsCount: Int {
        return viewModels.count
    }

    func viewModel(at index: Int) -> MovieViewModel? {
        guard index >= 0 && index < viewModelsCount else { return nil }
        loadNextPageIfNeeded(for: index)
        return viewModels[index]
    }
}

private extension MovieViewModelController {

    static func parse(_ jsonData: Data) -> [Movie?]? {
        let jsonValue = JSON(jsonData)
        
        if let movies = jsonValue["results"].array {
            var Movies: [Movie?] = []
            
            for movie in movies {
                if let poster_path = movie["poster_path"].string, let overview = movie["overview"].string, let release_date = movie["release_date"].string, let title = movie["title"].string {
                    let year = String(release_date.prefix(4))
                    let url = "\(API.BASE_IMAGE_URL)\(poster_path)"
                    Movies.append(Movie.init(poster_path: url, title: title, release_date: year, overview: overview))
                }
            }
            return Movies
        }
        do {
            return try JSONDecoder().decode([Movie].self, from: jsonData)
        } catch {
            return nil
        }
    }

    static func initViewModels(_ movies: [Movie?]) -> [MovieViewModel?] {
        return movies.map { movie in
            if let movie = movie {
                return MovieViewModel(movie: movie)
            } else {
                return nil
            }
        }
    }
    
    func getUrl() -> String {
        if(apiRoute == API.ROUTE_SEARCH) {
            return "\(API.BASE_API_URL)\(API.ROUTE_SEARCH)?\(API.PARAMETERS)&\(API.PARAMETERS_SEARCH)\(searchTerm)"
        } else {
            return "\(API.BASE_API_URL)\(API.ROUTE_POPULAR)?\(API.PARAMETERS)"
        }
    }

    func loadNextPageIfNeeded(for index: Int) {
        let targetCount = currentPage < 0 ? 0 : (currentPage + 1) * MovieViewModelController.pageSize - 4
        guard index == targetCount else {
            return
        }
        
        currentPage += 1
        
        let urlString = getUrl() + "&page=\(currentPage + 1)"

        guard let url = URL(string: urlString) else {
            retrieveMoviesCompletionBlock?(false, nil)
            return
        }
        let session = URLSession.shared
        let task = session.dataTask(with: url) { [weak self] (data, response, error) in
            guard let strongSelf = self else { return } // strongify optional self
            guard let jsonData = data, error == nil else {
                DispatchQueue.main.async {
                    strongSelf.retrieveMoviesCompletionBlock?(false, error as NSError?)
                }
                return
            }
            if let movies = MovieViewModelController.parse(jsonData) {
                let newMoviesPage = MovieViewModelController.initViewModels(movies)
                strongSelf.viewModels.append(contentsOf: newMoviesPage)
                DispatchQueue.main.async {
                    strongSelf.retrieveMoviesCompletionBlock?(true, nil)
                }
            } else {
                DispatchQueue.main.async {
                    strongSelf.retrieveMoviesCompletionBlock?(false, NSError.createError(0, description: "JSON parsing error"))
                }
            }
        }
        task.resume()
    }
}
