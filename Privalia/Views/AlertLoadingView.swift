//
//  AlertLoadingView.swift
//  Privalia
//
//  Created by Nil Puig on 29/03/2018.
//  Copyright © 2018 Nil Puig. All rights reserved.
//

import UIKit

public final class AlertLoadingView: UIView {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
}

